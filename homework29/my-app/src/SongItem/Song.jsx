import React from "react";
import hearth from "../images/like.svg";
import './Song.css'

export default function Song({ song, likeSong, removeSong }) {
  return (
    <>
      <li className={song.isLiked ? "true" : "false"}>
        {song.isLiked && <img src={hearth} alt="" className="like-icon" />}
        {song.name}
        <button onClick={() => likeSong(song.id)} className="button like">
          {song.isLiked ? "Unlike" : "Like"}
        </button>
        <button className="button delete" onClick={() => removeSong(song.id)}>
          Delete
        </button>
      </li>
    </>
  );
}
