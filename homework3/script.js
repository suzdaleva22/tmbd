//-----Умовні конструкції-домашка 3----
 //1) Напишіть програму, яка питає у користувача номер автобуса.Якщо це номер 7, 255 или 115, тоді користувач може їхати. Виведіть в консолі "You can go".Якщо ні - виведіть в консолі "Please wait".

let busNumber = +prompt(`What is the bus number?`);
    
if (isNaN(busNumber) || !Number.isInteger(busNumber) || busNumber <= 0) {
    console.error('You entered incorrect data');
} else if (busNumber == 7 || busNumber == 255 || busNumber == 115) {
    console.log('You can go');
} else {
    console.log('Please wait');
};

//2) Напишіть програму, яка отримує від користувача число і порівнює його з числом Pi. (Math.PI)
//You entered: <number> 
//Is greater then PI: true
//Is less then PI: false
//Is equal PI: false

let number = parseFloat(prompt(`Write any number`));
const piNumber = Math.PI;

console.log(`You entered: ${number}
Is equal PI: ${number === piNumber}
Is greater then PI: ${number > piNumber}
Is less then PI: ${number < piNumber}`)

//3) Напишіть програму, яка пропонує користувачу ввести пароль і перевіряє, чи є ций пароль надійним за наступними умовами:

// Пароль повинен бути не менше шести символів;
// Пароль не повинен бути рівним строкам qwerty чи 123456;
// Пароль повинен мати хоча б одну велику літеру.
// Якщо всі умови виконані, виведіть в консоль повідомлення "Strong".

// Якщо пароль має хоча б одну велику літеру але складається з п'яти символів, виведіть в консоль повідомлення "Middle".

// У всіх інших випадках, виведіть в консоль повідомлення "Weak".

let password = prompt(`Set password`);

if (password.length >= 6 && 
    password !== 'qwerty' && 
    password !== '123456' && 
    password.toLowerCase() !== password ) {
    console.log(`Strong`);
} else if (password.length = 5 && 
    password.toLowerCase() !== password ) {
    console.log(`Middle`);
} else {
    console.log(`Weak`); 
};

// 4) Напишіть програму, яка питає у користувача номер квартири (команда prompt()) і виводить в консоль номер поверху і номер під'їзду.

// Відомо, що в одному під'їзді 9 поверхів по 3 квартири на кожному поверсі. Результат (поверх і під'їзд) відобразіть в консолі (команда console.log()).

const roomsOnFloor = 3;
const floors = 9;
const roomNumber = prompt(`Type your room number`);

const roomsInEntrance = roomsOnFloor * floors;

let numberOfEntrance = (roomNumber /  roomsInEntrance);
console.log(`Entrance: `+ Math.ceil(numberOfEntrance));

let numberOfFloor =  (roomNumber  - (Math.floor(numberOfEntrance) * roomsInEntrance)) / roomsOnFloor ;  // задачка огонь, намучилась капец)))
console.log(`Floor: ` + Math.ceil (numberOfFloor));

// 5) Напишіть програму, яка питає у користувача температуру в Цельсіях и конвертує її в Фаренгейти. Результат відобразіть в консолі.

// Формула для конвертації: Цельсій х 1,8 + 32 = Фаренгейт

// tips: градус Цельсія в юнікоді буде "\u2103", щоб відобразити його в строці. Спробуйте самостійно знайти символ для позначення градусу Фаренгейта.

// 60°C is 140°F

const temperatureCelsius = prompt(`Type temperature in Celsius`);
const temperatureFahrenheit = temperatureCelsius * 1.8 + 32;

console.log(parseInt(temperatureCelsius) + `\u2103 is ` + parseInt(temperatureFahrenheit)+ `\u2109`);



    
